package mx.ms.eni.login.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.ms.eni.login.business.ILoginService;
import mx.ms.eni.login.model.dto.LoginDTO;
import mx.ms.eni.login.model.entity.LoginEntity;
import mx.ms.eni.login.model.repository.ILoginRepository;

@Service
public class LoginServiceImpl implements ILoginService{

	@Autowired
	ILoginRepository loginRepository = null; 
	
	@Autowired
	LoginDTO loginDTO = null;
	
	public LoginDTO getLogin(Integer idUser)
	{
		LoginEntity LoginEntity = loginRepository.findByIdUser(idUser);
		if(LoginEntity != null)
		{
			loginDTO = new LoginDTO();
			loginDTO.setIdLogin(LoginEntity.getIdLogin());
			loginDTO.setIdUser(LoginEntity.getIdUser());
			loginDTO.setDate(LoginEntity.getDate());
			loginDTO.setPassword(LoginEntity.getPassword());
		}
		return loginDTO;
	}
	
	
	public List<LoginDTO> getLoginByFecha(Date fecha)
	{
		List<LoginDTO> loginDTOList = null;
		List<LoginEntity> loginEntityList = loginRepository.findByFecha(fecha);
		if(loginEntityList != null)
		{
			loginDTOList = new ArrayList<LoginDTO>();
			for(LoginEntity loginEntity : loginEntityList) {
				loginDTO = new LoginDTO();
				loginDTO.setIdLogin(loginEntity.getIdLogin());
				loginDTO.setIdUser(loginEntity.getIdUser());
				loginDTO.setDate(loginEntity.getDate());
				loginDTOList.add(loginDTO);
			}
		}
		return loginDTOList;
	}
	
	public void newLogin(LoginDTO loginDTO)
	{
		LoginEntity loginEntity = new LoginEntity();
		loginEntity.setIdLogin(loginDTO.getIdLogin());
		loginEntity.setIdUser(loginDTO.getIdUser());
		loginEntity.setDate(loginDTO.getDate());
		loginEntity.setPassword(loginDTO.getPassword());
		loginRepository.save(loginEntity);
	}
}
