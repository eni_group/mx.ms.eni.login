package mx.ms.eni.login.business;

import java.util.Date;
import java.util.List;

import mx.ms.eni.login.model.dto.LoginDTO;

public interface ILoginService {

	LoginDTO getLogin(Integer idUser);
	
	List<LoginDTO> getLoginByFecha(Date fecha);
	
	void newLogin(LoginDTO oginDTO);

}
