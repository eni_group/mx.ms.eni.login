package mx.ms.eni.login.model.dto;

import java.util.Date;

public class LoginDTO {

	private Integer idLogin;
	
	private Integer idUser;
	
	private Date date;
	
	private Integer password;
	
	public LoginDTO() { 
		
	}

	public Integer getIdLogin() {
		return idLogin;
	}

	public void setIdLogin(Integer idLogin) {
		this.idLogin = idLogin;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getPassword() {
		return password;
	}

	public void setPassword(Integer password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "LoginDTO [idLogin=" + idLogin + ", idUser=" + idUser + ", date=" + date + ", password=" + password
				+ "]";
	}
}
