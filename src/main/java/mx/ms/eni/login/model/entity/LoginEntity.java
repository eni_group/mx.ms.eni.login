package mx.ms.eni.login.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Login")
public class LoginEntity implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idLogin")
	private Integer idLogin;
	
	@Column(name="Usuario_idUsuario", nullable = false)
	private Integer idUser;
	
	@Column(name="Fecha", nullable = false)
	private Date date;
	
	@Column(name="Password", nullable = false)
	private Integer password;
	
	public LoginEntity() {
		
	}

	public Integer getIdLogin() {
		return idLogin;
	}

	public void setIdLogin(Integer idLogin) {
		this.idLogin = idLogin;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date fecha) {
		this.date = fecha;
	}
	
	public Integer getPassword() {
		return password;
	}

	public void setPassword(Integer password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "LoginEntity [idLogin=" + idLogin + ", idUser=" + idUser + ", fecha=" + date + ", password=" + password
				+ "]";
	}
}
