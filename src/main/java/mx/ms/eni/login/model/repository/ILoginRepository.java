package mx.ms.eni.login.model.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.ms.eni.login.model.entity.LoginEntity;

@Repository
public interface ILoginRepository extends JpaRepository<LoginEntity, Integer>{
	
	LoginEntity findByIdUser(Integer idUser);
	
	List<LoginEntity> findByFecha(Date fecha);
}
