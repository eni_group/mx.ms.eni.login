package mx.ms.eni.login.service;

import java.util.Date;
import java.util.List;

import mx.ms.eni.login.model.dto.LoginDTO;

public interface ILoginPublicService {
	
	LoginDTO getLogin(Integer idUser);
	
	List<LoginDTO> getLoginByFecha(Date fecha);
	
	void newLogin(LoginDTO oginDTO);
}
