package mx.ms.eni.login.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.ms.eni.login.business.ILoginService;
import mx.ms.eni.login.model.dto.LoginDTO;
import mx.ms.eni.login.service.ILoginPublicService;

@RestController
@RequestMapping(value="/login")
public class LoginController implements ILoginPublicService{

	@Autowired
	ILoginService loginService = null;
	
	@Override
	@CrossOrigin(origins="*")
	@RequestMapping(value="/", produces="application/json", method = {RequestMethod.GET})
	public LoginDTO getLogin(@RequestParam Integer idUser)
	{
		return loginService.getLogin(idUser);
	}
	
	@Override
	@CrossOrigin(origins="*")
	@GetMapping(value="/", produces="application/json")
	public List<LoginDTO> getLoginByFecha(@RequestParam Date fecha)
	{
		return loginService.getLoginByFecha(fecha);
	}
	
	@Override
	@CrossOrigin(origins="*")
	@PostMapping(value="/", consumes = "application/json", produces="application/json")
	public void newLogin(@RequestBody LoginDTO loginDTO)
	{
		loginService.newLogin(loginDTO);
	}
}
